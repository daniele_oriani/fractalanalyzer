#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{   
    _planeManager = std::shared_ptr<PlaneManager>(new PlaneManager(QSize(1600, 1200))); //default picture size: 1600x1200; Proportions must be conserved;

    _fractalImage = _planeManager->getPic();

    _mandelbrot = std::shared_ptr<mandelbrotCalculator>(new mandelbrotCalculator(_planeManager.get()));
    _burningShip = std::shared_ptr<burningShipCalculator>(new burningShipCalculator(_planeManager.get()));
    _juliaSet = std::shared_ptr<juliaCalculator>(new juliaCalculator(_planeManager.get()));


    drawWindow(QSize(450, 370));
    makeConnections();
    loadPlaneSettings(); //load center, scale factor and picture size from the ui, and modify the plane accordingly;
}

void MainWindow::drawWindow(const QSize wndSize)
{
    //Resize mainwindow;
    this->setFixedSize(wndSize);
    this->setWindowTitle("Fractal Analyzer");
    //Do the actual drawing...
    QSize margin(4, 2);
    QString btnStyleSheet = "border: 1px solid #2b2b2b;"
                            "border-radius: 2px;"
                            "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde);";

    /*Top Menu*/
    _runAnalysis_pb.setParent(this);
    _runAnalysis_pb.setText("Run Analysis...");
    _runAnalysis_pb.setFlat(true);
    _runAnalysis_pb.setGeometry(0 + margin.width(), 0 + margin.height(), 110, 25);
    _runAnalysis_pb.setStyleSheet(btnStyleSheet);

    _showSettings_pb.setParent(this);
    _showSettings_pb.setText("Settings...");
    _showSettings_pb.setFlat(true);
    _showSettings_pb.setGeometry(_runAnalysis_pb.x() + _runAnalysis_pb.width() + 2, _runAnalysis_pb.y(), 80, 25);
    _showSettings_pb.setStyleSheet(btnStyleSheet);

    _showAbout_pb.setParent(this);
    _showAbout_pb.setText("About");
    _showAbout_pb.setFlat(true);
    _showAbout_pb.setGeometry(_showSettings_pb.x() + _showSettings_pb.width() + 2, _runAnalysis_pb.y(), 65, 25);
    _showAbout_pb.setStyleSheet(btnStyleSheet);

    _showAboutQt_pb.setParent(this);
    _showAboutQt_pb.setText("About Qt");
    _showAboutQt_pb.setFlat(true);
    _showAboutQt_pb.setGeometry(_showAbout_pb.x() + _showAbout_pb.width() + 2, _runAnalysis_pb.y(), 70, 25);
    _showAboutQt_pb.setStyleSheet(btnStyleSheet);

    _quit_pb.setParent(this);
    _quit_pb.setText("Quit");
    _quit_pb.setFlat(true);
    _quit_pb.setGeometry(wndSize.width() - margin.width() - 60, _runAnalysis_pb.y(), 60, 25);
    _quit_pb.setStyleSheet(btnStyleSheet);

    /*RadioButton Group (Fractal Selector)*/
    _selectMandelbrot_rb.setParent(this);
    _selectMandelbrot_rb.setText("Mandelbrot Set");
    _selectMandelbrot_rb.setChecked(true);
    _selectMandelbrot_rb.setGeometry(_runAnalysis_pb.x(), _runAnalysis_pb.y()+3*margin.height()+_runAnalysis_pb.height(), 135, 25);

    _selectJulia_rb.setParent(this);
    _selectJulia_rb.setText("Julia Set");
    _selectJulia_rb.setGeometry(wndSize.width() - 87 - margin.width(), _selectMandelbrot_rb.y(), 87, 25);

    _selectShip_rb.setParent(this);
    _selectShip_rb.setText("Burning Ship");
    _selectShip_rb.setGeometry((_selectJulia_rb.x() + (_selectMandelbrot_rb.x()+_selectMandelbrot_rb.width()))/2 - 113/2 - margin.width(), _selectMandelbrot_rb.y(), 113, 25);
    //Parameters
    _centerPoint_lab.setParent(this);
    _centerPoint_lab.setText("Center:");
    _centerPoint_lab.setGeometry(_showSettings_pb.x()/3, _selectMandelbrot_rb.y()+ 25*margin.height(), 75, 25);

    _center_Re_le.setParent(this);
    _center_Re_le.setText("0.0");
    _center_Re_le.setGeometry(_centerPoint_lab.x()+ _centerPoint_lab.width()+ margin.width(), _centerPoint_lab.y()-margin.height(), 135, 30);

    _imagPart_lab.setParent(this);
    _imagPart_lab.setGeometry(_center_Re_le.x()+_center_Re_le.width()+margin.width(), _center_Re_le.y(), 25, 25);
    _imagPart_lab.setText(" + i");

    _center_Im_le.setParent(this);
    _center_Im_le.setText("0.0");
    _center_Im_le.setGeometry(_imagPart_lab.x()+_imagPart_lab.width(), _center_Re_le.y(), 135, 30);

    _scale_lab.setParent(this);
    _scale_lab.setText("Scale:");
    _scale_lab.setGeometry(_centerPoint_lab.x(), _centerPoint_lab.y() + _centerPoint_lab.height()+ 5*margin.height(), 75, 25);

    _scale_spin.setParent(this);
    _scale_spin.setDecimals(2);
    _scale_spin.setValue(0.65);
    _scale_spin.setMinimum(0.05);
    _scale_spin.setMaximum(10000.0);
    _scale_spin.setSingleStep(0.1);
    _scale_spin.setGeometry(_center_Re_le.x(), _scale_lab.y(), 135, 30);

    _optimize_lab.setParent(this);
    _optimize_lab.setText("Optimize:");
    _optimize_lab.setGeometry(_scale_lab.x(), _scale_lab.y()+_scale_lab.height()+5*margin.height(), 75, 25);

    _optimize_cb.setParent(this);
    _optimize_cb.setChecked(true);
    _optimize_cb.setText("(Only affects Mandelbrot)");
    _optimize_cb.setGeometry(_center_Re_le.x(), _optimize_lab.y(), 205, 30);

    //Julia
    _juliaC_lab.setParent(this);
    _juliaC_lab.setText("c (Julia):");
    _juliaC_lab.setGeometry(_scale_lab.x(), _optimize_cb.y() + _optimize_lab.height() + 5*margin.height(), 75, 25);

    _JuliaC_Re_le.setParent(this);
    _JuliaC_Re_le.setEnabled(false);
    _JuliaC_Re_le.setText("0.0");
    _JuliaC_Re_le.setGeometry(_center_Re_le.x(), _juliaC_lab.y(), 135, 30);

    _imagPart2_lab.setParent(this);
    _imagPart2_lab.setText(" + i");
    _imagPart2_lab.setGeometry(_imagPart_lab.x(), _juliaC_lab.y(), 25,25);

    _JuliaC_Im_le.setParent(this);
    _JuliaC_Im_le.setEnabled(false);
    _JuliaC_Im_le.setText("0.0");
    _JuliaC_Im_le.setGeometry(_center_Im_le.x(), _juliaC_lab.y(), 135, 30);

    _juliaPow_lab.setParent(this);
    _juliaPow_lab.setText("Pow (Julia):");
    _juliaPow_lab.setGeometry(_juliaC_lab.x(), _juliaC_lab.y()+ _juliaC_lab.height()+ 5*margin.height(), 75, 25);

    _JuliaPow_spin.setParent(this);
    _JuliaPow_spin.setDecimals(2);
    _JuliaPow_spin.setValue(2.0);
    _JuliaPow_spin.setMinimum(2.0);
    _JuliaPow_spin.setMaximum(100.0);
    _JuliaPow_spin.setEnabled(false);
    _JuliaPow_spin.setGeometry(_scale_spin.x(), _juliaPow_lab.y(), _scale_spin.width(), _scale_spin.height());
    /*Bottom Buttons*/
    _drawFractal_pb.setParent(this);
    _drawFractal_pb.setText("Draw!");
    _drawFractal_pb.setGeometry(this->width()/2-148, _JuliaPow_spin.y()+ _JuliaPow_spin.height() + 20*margin.height(), 145, 40);

    _addToAnalysis_pb.setParent(this);
    _addToAnalysis_pb.setText("Add to Analysis!");
    _addToAnalysis_pb.setGeometry(_drawFractal_pb.x()+_drawFractal_pb.width()+2*margin.width(), _drawFractal_pb.y(), 145, 40);
    /*Status Label*/
    _status_lab.setParent(this);
    _status_lab.setText("Ready!");
    _status_lab.setIndent(9);
    _status_lab.setStyleSheet("background-color: #d6d6d6; border-top: 1px solid #c2c2c2;");
    _status_lab.setGeometry(0, this->height() - 25, this->width(), 25);
    //Add radiobuttons to a button group, in order to have control over all of them at the same time.
    _fractType_radioButtons.setParent(this);
    _fractType_radioButtons.addButton(&_selectMandelbrot_rb, 0);
    _fractType_radioButtons.addButton(&_selectShip_rb, 1);
    _fractType_radioButtons.addButton(&_selectJulia_rb, 2);
}
void MainWindow::makeConnections()
{   
    connect(&_showAboutQt_pb, SIGNAL(clicked()), qApp, SLOT(aboutQt()));
    connect(&_quit_pb, SIGNAL(clicked()), qApp, SLOT(quit()));

    connect(&_center_Re_le, SIGNAL(textChanged(QString)), this, SLOT(validateText(QString)));
    connect(&_JuliaC_Re_le, SIGNAL(textChanged(QString)), this, SLOT(validateText(QString)));
    connect(&_JuliaC_Im_le, SIGNAL(textChanged(QString)), this, SLOT(validateText(QString)));

    connect(&_showSettings_pb, SIGNAL(clicked()), &_settings, SLOT(exec()));
    connect(&_runAnalysis_pb, SIGNAL(clicked()), &_analysis, SLOT(exec()));
    connect(&_showAbout_pb, SIGNAL(clicked()), this, SLOT(aboutBtn_clicked()));

    connect(&_fractType_radioButtons, SIGNAL(buttonClicked(int)), this, SLOT(fractTypeChanged(int)));

    connect(&_drawFractal_pb, SIGNAL(clicked()), this, SLOT(drawBtn_clicked()));
    connect(&_addToAnalysis_pb, SIGNAL(clicked()), this, SLOT(addBtn_clicked()));
    connect(&_addToAnalysis_pb, SIGNAL(clicked()), &_analysis, SLOT(showElementsList()));

    connect(_mandelbrot.get(), SIGNAL(doneComputing()), this, SLOT(fractalComputed()));
    connect(_burningShip.get(), SIGNAL(doneComputing()), this, SLOT(fractalComputed()));
    connect(_juliaSet.get(), SIGNAL(doneComputing()), this, SLOT(fractalComputed()));
}

bool MainWindow::validateText(const QString& newTxt)
{
    //As soon as the user enters some text in the textboxes, we parse it to see if it is formatted correctly.
    QRegExp doubleTest("-?\\d+(\\.|$)\\d*");
    //<==> [minus] digits (either . or end of line) digits
    if(!doubleTest.exactMatch(newTxt) && newTxt != "-")
    {
        QMessageBox::warning(this, "Formatting error", "You tried to enter an invalid number in the text box.", QMessageBox::Ok, QMessageBox::Cancel);
        return false;
    }
    return true;
}

bool MainWindow::loadPlaneSettings()
{
    bool res = validateText(_center_Re_le.text()) & validateText(_center_Im_le.text()); //if there's some text that is not formatted properly, fail.
    if(res)
    {
        _planeManager->setCenter(_center_Re_le.text().toDouble(), _center_Im_le.text().toDouble());
    }
    _planeManager->scale(_scale_spin.value());
    _planeManager->resize(_settings.getPicSize());

    _fractalImage = 0; //resizing causes creation of a new image. Resetting the pointer to 0 avoids dangling pointers!

    return res;
}

bool MainWindow::loadFractalParameters()
{
    bool success = false;
    if(_selectMandelbrot_rb.isChecked())
    {
        success = loadMandelbrotParameters();
    }
    else if(_selectShip_rb.isChecked())
    {
        success = loadShipParameters();
    }
    else if(_selectJulia_rb.isChecked())
    {
        success = loadJuliaParameters();
    }
    return success;
}
bool MainWindow::loadMandelbrotParameters()
{
    _mandelbrot->setBailoutRadius(_settings.getBailoutRadius());
    _mandelbrot->setDepth(_settings.getDepth());
    _mandelbrot->setOptimized(_optimize_cb.isChecked());
    _mandelbrot->getColorizer()->setBw(_settings.getUseBlackWhite());
    _mandelbrot->getColorizer()->setUseGradient(_settings.getUseGradient());
    return true; //the only thing that could fail is reading the parameters for the Julia Set.
}
bool MainWindow::loadShipParameters()
{
    _burningShip->setBailoutRadius(_settings.getBailoutRadius());
    _burningShip->setDepth(_settings.getDepth());
    _burningShip->getColorizer()->setBw(_settings.getUseBlackWhite());
    _burningShip->getColorizer()->setUseGradient(_settings.getUseGradient());
    return true; //the only thing that could fail is reading the parameters for the Julia Set.
}
bool MainWindow::loadJuliaParameters()
{
    _juliaSet->setBailoutRadius(_settings.getBailoutRadius());
    _juliaSet->setDepth(_settings.getDepth());
    _juliaSet->setPower(_JuliaPow_spin.value());
    _juliaSet->getColorizer()->setBw(_settings.getUseBlackWhite());
    _juliaSet->getColorizer()->setUseGradient(_settings.getUseGradient());
    bool success = validateText(_JuliaC_Re_le.text()) & validateText(_JuliaC_Im_le.text());
    if(success) _juliaSet->setC(_JuliaC_Re_le.text().toDouble(), _JuliaC_Im_le.text().toDouble());
    return success;
}
void MainWindow::drawBtn_clicked()
{
    bool status_ok = loadPlaneSettings() & loadFractalParameters();
    if(!status_ok)
    {
        QMessageBox::critical(this, "Input error", "There was an error reading from settings and parameters."
                              "\nPlease make sure you entered correctly formatted input.\nComputation cannot start for now.");
        updateStatus("Number formatting error!");
        return;
    }
    //if we get here: all good ==> start computation!
    _drawFractal_pb.setEnabled(false);
    if(_selectMandelbrot_rb.isChecked())
    {
        updateStatus("Computing Mandelbrot Set, please wait...");
        _mandelbrot->start(); //<==> compute();
    }
    else if(_selectShip_rb.isChecked())
    {
        updateStatus("Computing Burning Ship, please wait...");
        _burningShip->start(); //<==> compute();
    }
    else if(_selectJulia_rb.isChecked())
    {
        updateStatus("Computing Julia Set, please wait...");
        _juliaSet->start(); //<==> compute();
    }
    else
    {
        //should never be reached;
        std::cerr << "No computation to be done.\n";
        updateStatus("No computation to be done.");
        return;
    }
    //When the computation of each fractal ends, a signal is emitted, which is connected to void fractalComputed(), below;
}
void MainWindow::fractalComputed()
{
    _fractalImage = _planeManager->getPic(); //gets us a pointer to work with the fresh picture;

    if(_selectMandelbrot_rb.isChecked())
    {
        updateStatus("Mandelbrot Set computed successfully!");
    }
    else if(_selectShip_rb.isChecked())
    {
        updateStatus("Burning Ship computed successfully!");
    }
    else if(_selectJulia_rb.isChecked())
    {
        updateStatus("Julia Set computed successfully!");
    }
    QMessageBox msgBox;
    msgBox.setText("Computation completed successfully!");
    msgBox.setInformativeText("Do you wish to save the fractal picture?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
    msgBox.setDefaultButton(QMessageBox::Save);

    int ans = msgBox.exec();
    QString fileName;
    switch(ans)
    {
        case QMessageBox::Save:
            fileName = QFileDialog::getSaveFileName(this, "Save Image", "/home/", "Images (*.png *.xpm *.jpg)");
            if(_selectShip_rb.isChecked())
            {
                //this fractal is usually represented upsidedown...
                _fractalImage->mirrored().save(fileName);
            }
            else
            {
                _fractalImage->save(fileName);
            }
            updateStatus("Picture saved successfully!");
            break;
        case QMessageBox::Discard:
            break;
        default:
            break; //should never be reached;
    }
    _drawFractal_pb.setEnabled(true);
}
void MainWindow::fractTypeChanged(int newFractType)
{
    switch(newFractType)
    {
    case 2: //julia set was selected;
        _JuliaC_Re_le.setEnabled(true);
        _JuliaC_Im_le.setEnabled(true);
        _JuliaPow_spin.setEnabled(true);
        break;
    default: //anything else was selected;
        _JuliaC_Re_le.setEnabled(false);
        _JuliaC_Im_le.setEnabled(false);
        _JuliaPow_spin.setEnabled(false);
        break;
    }
}
void MainWindow::addBtn_clicked()
{
    fractalAnalysisSetUp fractSettings = _settings.getSettings();
    planeSetUp planeSettings;
    if(_selectMandelbrot_rb.isChecked())
    {
        fractSettings.type = fractalAnalysisSetUp::fractalType::Mandelbrot;
        fractSettings.optimize = _optimize_cb.isChecked();
    }
    else if(_selectShip_rb.isChecked())
    {
        fractSettings.type = fractalAnalysisSetUp::fractalType::BurningShip;
    }
    else if(_selectJulia_rb.isChecked())
    {
        fractSettings.type = fractalAnalysisSetUp::fractalType::JuliaSet;
    }
    else
    {
        //should never be reached;
        std::cerr << "No fractal type selected.\n";
        updateStatus("No fractal type selected.");
        return;
    }
    bool res = validateText(_center_Re_le.text()) & validateText(_center_Im_le.text());
    if(res)
    {
        QPointF center(_center_Re_le.text().toDouble(), _center_Im_le.text().toDouble());
        planeSettings.center = center;
    }else
    {
        QMessageBox::critical(this, "Input error", "There was an error reading from settings and parameters."
                              "\nPlease make sure you entered correctly formatted input.\nCannot add this computation for now.");
        updateStatus("Number formatting error!");
        return;
    }
    planeSettings.scale = _scale_spin.value();

    _analysis.addSetUpElement(fractSettings, planeSettings);
    updateStatus("Successfully added current fractal.");
}
void MainWindow::aboutBtn_clicked()
{
    QMessageBox::information(this, "About", "Fractal Analyzer 1.0"
                             "\nAuthor: Daniele Oriani"
                             "\nContact: daniele.oriani@studenti.unimi.it"
                             "\nCopyright: 2015 Daniele Oriani. All rights reserved."
                             "\n\nSome useful values for 'c' in the Julia Set:"
                             "\n(-0.4, 0.6);  (0.285, 0.0);  (0.285, 0.01);  (0.45, 0.1428);"
                             "\n(-0.70176, -0.3842);  (-0.835, -0.2321);  (-0.8, 0.156);  (0.4, 0.0)");
}
MainWindow::~MainWindow()
{
    if(_mandelbrot->isRunning()) _mandelbrot->exit(0);
    if(_burningShip->isRunning()) _burningShip->exit(0);
    if(_juliaSet->isRunning()) _juliaSet->exit(0);
}
