#include "mandelbrotcalculator.h"

mandelbrotCalculator::mandelbrotCalculator(PlaneManager *m)
    : fractalCalculator(m)
{
    _optimize = true;
    _colorizer = std::shared_ptr<colorCalculator>(new colorCalculator(_depth));
}
mandelbrotCalculator::mandelbrotCalculator(mandelbrotCalculator& other)
    : fractalCalculator(other.getPlaneManager())
{
    _depth = other.getDepth();
    _bailoutRadius = other.getBailoutRadius();
    _colorizer = std::shared_ptr<colorCalculator>(new colorCalculator(_depth));
    _manager = other.getPlaneManager();
    _bailoutSq = _bailoutRadius*_bailoutRadius;
}
void mandelbrotCalculator::compute()
{
    QRgb color;
    QPointF c(0.0, 0.0);
    int width = _manager->getSize().width(), height = _manager->getSize().height();
    int it_num = 0;
    double zn = 0.0;
    //for each pixel (px, py) we map it to a point in the complex plane, using the plane manager. That point is the starting point of our iteration.
    for(int x = 0; x < width; ++x)
    {
            for(int y = 0; y < height; ++y)
            {
                c = _manager->mapFromPic(x, y);
                it_num = computePixel(c.x(), c.y(), &zn);
                color = _colorizer->computeColor(it_num, zn);
                _manager->colorPixel(x, y, color);
            }
    }
    emit doneComputing();
}

int mandelbrotCalculator::computePixel(const double& c_real, const double& c_im, double* zn)
{
    //zn is the |z| of the final iteration point. It is needed by the color calculator;
    *zn = 0.0;
    double z0_real = 0, z1_real = 0.0, z1_imag = 0.0, modSquared = 0.0;
    if(_optimize)
    {
        //Optimization: detection of points lying int the main cardioid or in the secondary bulb;
        double p1 = (c_real - 0.25)*(c_real - 0.25) + c_im*c_im;
        double p2 = (c_real + 1)*(c_real + 1) + c_im*c_im;
        if((p1*(p1+(c_real-0.25) < 0.25*c_im*c_im)) || (p2 < 0.0625))//if we are in a black zone...
            return _depth; //...make it black, without iterating;
    }
    //if we're not optimizing, let's start iterating in any case.
    for(int it_num = 1; it_num <= _depth; ++it_num)
    {
        z0_real = z1_real;
        z1_real = z1_real*z1_real - z1_imag*z1_imag + c_real; //z = z0^2 + c ==>(Re) Re(z) = Re(z0)^2 - Im(z0)^2 + Re(c)
        z1_imag = 2.0*z0_real*z1_imag + c_im; //z = z0^2 + c ==>(Im) Im(z) = 2*Re(z0)*Im(z0) + Im(c);
        modSquared = z1_real*z1_real + z1_imag*z1_imag;
        if(modSquared > _bailoutSq)
        {
            *zn = std::sqrt(modSquared);
            return it_num;
        }
    }
    return _depth;
}

mandelbrotCalculator::~mandelbrotCalculator()
{

}
