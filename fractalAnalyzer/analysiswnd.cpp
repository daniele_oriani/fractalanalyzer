#include "analysiswnd.h"
analysisWnd::analysisWnd(QWidget *parent) :
    QDialog(parent)
{
    drawWnd();
    _savePath = QString();
    _savePics = false;

    _x_type = IndependentVariable::N;

    makeConnections();

    showElementsList();
}
void analysisWnd::makeConnections()
{
    connect(&removeLast_pb, SIGNAL(clicked()), this, SLOT(removeLast()));
    connect(&clear_pb, SIGNAL(clicked()), this, SLOT(clear()));
    connect(&run_pb, SIGNAL(clicked()), this, SLOT(run()));
    connect(&_choosex_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(setIndependentVar(int)));
}
void analysisWnd::drawWnd()
{
    this->setFixedSize(370, 380);
    this->setWindowTitle("Run Analysis");

    analysisTextInfo_txt.setParent(this);
    analysisTextInfo_txt.setReadOnly(true);
    analysisTextInfo_txt.setGeometry(0,0, this->width(), this->height()-100);

    _exportData_cb.setParent(this);
    _exportData_cb.setText("Export Data");
    _exportData_cb.setChecked(true);
    _exportData_cb.setGeometry(10, analysisTextInfo_txt.height()+5, 110, 20);

    _variable_lab.setParent(this);
    _variable_lab.setText("Indipendent var:");
    _variable_lab.setGeometry(_exportData_cb.x()+_exportData_cb.width()+5, _exportData_cb.y(), 110, 20);

    _choosex_combo.setParent(this);
    _choosex_combo.setGeometry(_variable_lab.x()+_variable_lab.width()+10, _exportData_cb.y(), 110, 27);
    _choosex_combo.addItem("N"); _choosex_combo.addItem("Bailout r"); _choosex_combo.addItem("Depth"); _choosex_combo.addItem("Width"); _choosex_combo.addItem("Scale f");
    /*                      0                                    1                              2                               3                           4               */

    removeLast_pb.setParent(this);
    removeLast_pb.setText("Remove Last");
    removeLast_pb.setGeometry(10, this->height()-40, 100, 35);

    clear_pb.setParent(this);
    clear_pb.setText("Clear");
    clear_pb.setGeometry(removeLast_pb.x()+removeLast_pb.width()+10, removeLast_pb.y(), 100, 35);

    run_pb.setParent(this);
    run_pb.setText("Run!");
    run_pb.setGeometry(clear_pb.x()+clear_pb.width()+10, clear_pb.y(), 100, 35);
}
QString analysisWnd::getSetupInfo(const std::tuple<fractalAnalysisSetUp, planeSetUp> &setup)
{
    fractalAnalysisSetUp settings;
    planeSetUp planeSettings;

    std::tie(settings, planeSettings) = setup; //split up the couple;

    QString res; //our setup info;

    switch(settings.type)
    {
    case fractalAnalysisSetUp::fractalType::Mandelbrot:
         res = "Mandelbrot Set:\n";
         break;
    case fractalAnalysisSetUp::fractalType::BurningShip:
        res = "Burning Ship Fractal:\n";
        break;
    case fractalAnalysisSetUp::fractalType::JuliaSet:
        res = "Julia Set:\n";
        break;
    default:
        res = "Error!";
        return res;
        break;
    }
    res += QString("         Depth == "); res += QString::number(settings.depth); res += QString(",\n");
    res += QString("         BailoutRadius == "); res += QString::number(settings.bailout); res += QString(",\n");
    res += QString("         Gradient == "); res += boolToStr(settings.useGradient); res += QString(",\n");
    res += QString("         Black&White == "); res += boolToStr(settings.bw); res += QString(",\n");
    if(settings.type == fractalAnalysisSetUp::fractalType::Mandelbrot)
    {
        res += QString("         Optimized == "); res += boolToStr(settings.optimize); res += QString(",\n");
    }
    res += QString("         Center == ("); res += QString::number(planeSettings.center.x()) + QString(", ") + QString::number(planeSettings.center.y()); res += QString("),\n");
    res += QString("         Scale == "); res += QString::number(planeSettings.scale); res += QString(",\n");
    res += QString("         Size == "); res += QString::number(settings.picSize.width()); res += "x"; res += QString::number(settings.picSize.height());

    res += QString("\n\n");

    return res;
}
void analysisWnd::showElementsList()
{
    if(_setups.size() > 0)
    {
        QString txt;
        for(unsigned int i=0; i<_setups.size(); ++i)
        {
            txt += QString::number(1+i) + QString(") ") + getSetupInfo(_setups.at(i));
        }
        txt.chop(1); //remove extra \n char;
        analysisTextInfo_txt.setPlainText(txt);
    }
    else
    {
        analysisTextInfo_txt.setPlainText("Empty...");
    }
}

void analysisWnd::run()
{
    if(_setups.size()<=0) return; //if there are no configs to analyze, do nothing;

    bool abortAnalysis = false;
    showSavePicsDialog(&abortAnalysis);
    if(abortAnalysis) return;

    _results.clear();
    analysisTextInfo_txt.clear();
    removeLast_pb.setEnabled(false);
    run_pb.setEnabled(false);
    clear_pb.setEnabled(false);

    startNextComputation(0u);
}
void analysisWnd::startNextComputation(unsigned int computationIndex)
{
    QString status = analysisTextInfo_txt.toPlainText(); //current status;
    fractalAnalysisSetUp settings;
    planeSetUp planeSettings;

    std::tie(settings, planeSettings) = _setups.at(computationIndex); //split up the couple;

    //set up the plane, according to the config;
    _planeManager.scale(planeSettings.scale);
    _planeManager.resize(settings.picSize);
    _planeManager.setCenter(planeSettings.center);

    //depending on the fract type, create the fractalCalculator;
    //Then connect its doneComputing signal to the fractalComputed slot;
    switch(settings.type)
    {
    case fractalAnalysisSetUp::fractalType::Mandelbrot:
    {
        mandelbrotCalculator* myMandelbrot = new mandelbrotCalculator(&_planeManager);
        myMandelbrot->setOptimized(settings.optimize);
        _currentFract.reset(myMandelbrot); //deletes managed object and sets myMandelbrot as the new object
        connect(_currentFract.get(), SIGNAL(doneComputing()), this, SLOT(fractalComputed()));
        break;
    }
    case fractalAnalysisSetUp::fractalType::BurningShip:
        _currentFract.reset(new burningShipCalculator(&_planeManager)); //deletes previous object and acquires new ptr;
        connect(_currentFract.get(), SIGNAL(doneComputing()), this, SLOT(fractalComputed()));
        break;
    case fractalAnalysisSetUp::fractalType::JuliaSet:
        _currentFract.reset(new juliaCalculator(&_planeManager)); //deletes previous object and acquires new ptr;
        connect(_currentFract.get(), SIGNAL(doneComputing()), this, SLOT(fractalComputed()));
        break;
    default:
        //should never reach here;
        status += QString("Cannot compute fractal no. ") + QString::number(computationIndex+1) + QString(". An error occurred (this fractal has no type?)");
        analysisTextInfo_txt.setPlainText(status);
        disconnect(_currentFract.get(), SIGNAL(doneComputing()), this, SLOT(fractalComputed()));
        if(computationIndex+1 < _setups.size())
        {
            startNextComputation(computationIndex+1);
        }
        else
        {
            _currentFract.reset();
        }
        return;
        break;
    }
    //set up current fractal with parameters from the current config;
    _currentFract->setBailoutRadius(settings.bailout);
    _currentFract->setDepth(settings.depth);
    _currentFract->getColorizer()->setBw(settings.bw);
    _currentFract->getColorizer()->setUseGradient(settings.useGradient);

    //update status;
    status += QString("Computing fractal no. ") + QString::number(computationIndex+1) + QString("... ");
    analysisTextInfo_txt.setPlainText(status);

    //start stopwatch;
    _clock_start = std::chrono::steady_clock::now();
    //start computation of current fractal config;
    _currentFract->start();
    //when done will call void fractalComputed() below;
}
void analysisWnd::fractalComputed()
{
    //stop stopwatch and calculate duration;
    _clock_end = std::chrono::steady_clock::now();
    std::chrono::steady_clock::duration Delta_t = _clock_end - _clock_start;
    double secs = double(Delta_t.count()) * std::chrono::steady_clock::period::num / std::chrono::steady_clock::period::den;
    //save time in results;
    _results.push_back(secs);
    //update status;
    QString txt = analysisTextInfo_txt.toPlainText();
    txt += QString("Done! (") + QString::number(secs) + QString(" s)\n");
    analysisTextInfo_txt.setPlainText(txt);
    //save pic if user wants to;
    if(_savePics) savePic(_results.size() - 1);

    disconnect(_currentFract.get(), SIGNAL(doneComputing()), this, SLOT(fractalComputed()));
    _currentFract.reset();

    if(_results.size() < _setups.size())
    {
        startNextComputation(_results.size());
    }
    else
    {
        txt += QString("--Analysis completed!");
        analysisTextInfo_txt.setPlainText(txt);
        removeLast_pb.setEnabled(true);
        run_pb.setEnabled(true);
        clear_pb.setEnabled(true);
        if(_exportData_cb.isChecked())
            getDataReadyToExport();
    }
}
void analysisWnd::savePic(unsigned int fractalIndex)
{
    fractalAnalysisSetUp settings = std::get<0>(_setups.at(fractalIndex));
    QString filename = _savePath + QString("/fract_") + QString::number(fractalIndex) + QString(".png");
    //make sure we are writing on a new file.
    QString extra_id("new_");
    while(QFile::exists(filename))
    {
        filename = _savePath + QString("/fract_") + extra_id + QString::number(fractalIndex) + QString(".png");
        extra_id += QString("new_");
    }

    if(settings.type == fractalAnalysisSetUp::fractalType::BurningShip)
    {
        _planeManager.getPic()->mirrored().save(filename);
    }
    else
    {
        _planeManager.getPic()->save(filename);
    }
}
void analysisWnd::showSavePicsDialog(bool* abortAnalysis)
{
    _savePath.clear();
    _savePics = false;
    *abortAnalysis = false;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Save pictures?");
    msgBox.setText("Your analysis is about to start.\nDo you wish to save the fractal pictures produced in the analysis?");
    msgBox.setInformativeText("If you choose \"Save\", the pictures will be placed in the directory you select.");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Ignore | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);

    int ans = msgBox.exec();
    switch(ans)
    {
        case QMessageBox::Save:
        _savePath = QFileDialog::getExistingDirectory(this, "Open Directory",
                                                         "/home/",
                                                         QFileDialog::ShowDirsOnly
                                                         | QFileDialog::DontResolveSymlinks);
        _savePics = true;
            break;
        case QMessageBox::Ignore:
            break;
        case QMessageBox::Cancel:
        *abortAnalysis = true;
            break;
        default:
            break; //should never be reached;
    }
}
void analysisWnd::getDataReadyToExport()
{
    std::vector<double> x, t; //x=independent variable;
    t = _results;

    fractalAnalysisSetUp settings;
    planeSetUp planeSettings;

    switch (_x_type)
    {
    case IndependentVariable::N:
    {
        for(unsigned int N = 1; N<=_setups.size(); ++N) x.push_back(static_cast<double>(N));
        break;
    }
    case IndependentVariable::Bailout:
        for(auto it = _setups.cbegin(); it != _setups.cend(); ++it)
        {
            std::tie(settings, std::ignore) = *it;
            x.push_back(settings.bailout);
        }
        break;
    case IndependentVariable::Depth:
        for(auto it = _setups.cbegin(); it != _setups.cend(); ++it)
        {
            std::tie(settings, std::ignore) = *it;
            x.push_back(settings.depth);
        }
        break;
    case IndependentVariable::Width:
        for(auto it = _setups.cbegin(); it != _setups.cend(); ++it)
        {
            std::tie(settings, std::ignore) = *it;
            x.push_back(settings.picSize.width());
        }
        break;
    case IndependentVariable::ScaleFactor:
        for(auto it = _setups.cbegin(); it != _setups.cend(); ++it)
        {
            std::tie(std::ignore, planeSettings) = *it;
            x.push_back(planeSettings.scale);
        }
        break;
    default:
        //should never reach here;
        break;
    }
    exportData(x, t);
}
void analysisWnd::exportData(const std::vector<double>& x, const std::vector<double>& t)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Save data?");
    msgBox.setText("Your data is ready.\nWhere do you wish to save it?");
    msgBox.setInformativeText("The program fractalDataPlotter will then be used to plot it."
                              "\nTo compare data sets you should run fractalDataPlotter separately and"
                              "specify as arguments the files containing the data to be plotted.");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Ignore);
    msgBox.setDefaultButton(QMessageBox::Save);

    int ans = msgBox.exec();
    QString fileName;
    switch(ans)
    {
        case QMessageBox::Save:
            fileName = QFileDialog::getSaveFileName(this, "Save File", "/home/");
            break;
        case QMessageBox::Ignore:
            return;
            break;
        default:
            return;
            break;
    }

    std::ofstream writer(fileName.toStdString());
    unsigned int counter = 0;
    while(writer.good() && counter < t.size())
    {
        writer << x.at(counter) << " " << t.at(counter) << std::endl;
        ++counter;
    }
    writer.close();
    if(!writer.fail())
        startPlotter(fileName);
}
void analysisWnd::startPlotter(const QString& filename)
{
    QString plotter = qApp->applicationDirPath() + QString("/fractalDataPlotter");
    QString indepVar_label;
    switch(_x_type)
    {
    case IndependentVariable::N:
        indepVar_label = "N";
        break;
    case IndependentVariable::Bailout:
        indepVar_label = "Bailout_Radius";
        break;
    case IndependentVariable::Depth:
        indepVar_label = "Depth";
        break;
    case IndependentVariable::Width:
        indepVar_label = "Width";
        break;
    case IndependentVariable::ScaleFactor:
        indepVar_label = "Zoom";
        break;
    default:
        //should never reach here
        break;
    }
    QString x_label = QString("--xlabel=") + indepVar_label;
    QString y_label = "--ylabel=time";

    /*fork() and execlp does not work here: I'm having multithreading issues... Using QProcess instead.*/

    QProcess plotterProcess;
    plotterProcess.startDetached(plotter, QStringList() << x_label << y_label << filename);
}
void analysisWnd::setIndependentVar(int newVar)
{
    if(newVar == 0)
    {
        _x_type = IndependentVariable::N;
    }
    else if(newVar == 1)
    {
        _x_type = IndependentVariable::Bailout;
    }
    else if(newVar == 2)
    {
        _x_type = IndependentVariable::Depth;
    }
    else if(newVar == 3)
    {
        _x_type = IndependentVariable::Width;
    }
    else if(newVar == 4)
    {
        _x_type = IndependentVariable::ScaleFactor;
    }
}
analysisWnd::~analysisWnd()
{
    if(_currentFract && _currentFract->isRunning()) _currentFract->exit(0);
}
