#-------------------------------------------------
#
# Project created by QtCreator 2014-12-10T22:23:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fractalAnalyzer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    settingswnd.cpp \
    fractalcalculator.cpp \
    mandelbrotcalculator.cpp \
    gradient.C \
    colorcalculator.cpp \
    burningshipcalculator.cpp \
    juliacalculator.cpp \
    analysiswnd.cpp \
    planemanager.cpp

HEADERS  += mainwindow.h \
    settingswnd.h \
    fractalcalculator.h \
    mandelbrotcalculator.h \
    colorcalculator.h \
    burningshipcalculator.h \
    juliacalculator.h \
    analysiswnd.h \
    fractalAnalysisSetUp.h \
    planemanager.h

QMAKE_CXXFLAGS += -std=c++11
QMAKE_LFLAGS += -std=c++11
