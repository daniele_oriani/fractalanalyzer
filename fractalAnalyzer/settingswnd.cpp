#include "settingswnd.h"

settingsWnd::settingsWnd(QWidget *parent) :
    QDialog(parent), _picSizeProportion(1.33333)
{
    _depth = 1000;
    _bailout = 512.0;
    _useGradient = true;
    _bw = false;
    _picSize = QSize(1600, 1200);

    this->setWindowTitle("Settings");
    this->setFixedSize(340, 310);

    drawWnd();
    makeConnections();
}
void settingsWnd::drawWnd()
{
    int padX = 25, padY = 18;
    _depth_lab.setParent(this);
    _depth_lab.setText("Depth:");
    _depth_lab.setGeometry(padX, padY, 100, 30);

    _depth_sb.setParent(this);
    _depth_sb.setMaximum(10000000);
    _depth_sb.setMinimum(50);
    _depth_sb.setSingleStep(50);
    _depth_sb.setValue(_depth);
    _depth_sb.setGeometry(_depth_lab.x() + _depth_lab.width() + padX, _depth_lab.y(), 160, 30);

    _bailout_lab.setParent(this);
    _bailout_lab.setText("Bailout Radius:");
    _bailout_lab.setGeometry(padX, padY+ _depth_lab.y() + _depth_lab.height(), 100, 30);

    _bailout_sb.setParent(this);
    _bailout_sb.setMaximum(10000000);
    _bailout_sb.setMinimum(16);
    _bailout_sb.setSingleStep(16);
    _bailout_sb.setValue(_bailout);
    _bailout_sb.setGeometry(_bailout_lab.x() + _bailout_lab.width() + padX, _bailout_lab.y(), 160, 30);

    _picSize_lab.setParent(this);
    _picSize_lab.setText("Picture Size (px):");
    _picSize_lab.setGeometry(padX, padY+ _bailout_lab.y() + _bailout_lab.height(), 113, 30);

    _pic_width_sb.setParent(this);
    _pic_width_sb.setMaximum(9000);
    _pic_width_sb.setMinimum(2);
    _pic_width_sb.setValue(_picSize.width());
    _pic_width_sb.setGeometry(_bailout_sb.x(), _picSize_lab.y(), 75, 30);

    _pic_height_sb.setParent(this);
    _pic_height_sb.setMaximum(6569);
    _pic_height_sb.setMinimum(1);
    _pic_height_sb.setValue(_picSize.height());
    _pic_height_sb.setGeometry(_bailout_sb.x() + _bailout_sb.width() -_pic_width_sb.width(), _picSize_lab.y(), 75, 30);

    _useGradient_lab.setParent(this);
    _useGradient_lab.setText("Use gradient:");
    _useGradient_lab.setGeometry(padX, padY+ _picSize_lab.y() + _picSize_lab.height(), 113, 30);

    _useGradient_cb.setParent(this);
    _useGradient_cb.setText("");
    _useGradient_cb.setChecked(_useGradient);
    _useGradient_cb.setGeometry(_pic_width_sb.x(), _useGradient_lab.y(), 20, 30);

    _blackWhite_lab.setParent(this);
    _blackWhite_lab.setText("Black & White:");
    _blackWhite_lab.setGeometry(padX, padY+ _useGradient_lab.y() + _useGradient_lab.height(), 113, 30);

    _black_white_cb.setParent(this);
    _black_white_cb.setText("");
    _black_white_cb.setChecked(_bw);
    _black_white_cb.setGeometry(_useGradient_cb.x(), _blackWhite_lab.y(), 20, 30);

    _apply_btn.setParent(this);
    _apply_btn.setText("Apply");
    _apply_btn.setGeometry(_bailout_sb.x() + _bailout_sb.width() - 75, _blackWhite_lab.y()+ _blackWhite_lab.height() + padY, 75, 29);

    _close_btn.setParent(this);
    _close_btn.setText("Close");
    _close_btn.setGeometry(_apply_btn.x()- padX/2 - 75, _apply_btn.y(), 75, 29);
}

void settingsWnd::makeConnections()
{
    connect(&_close_btn, SIGNAL(clicked()), this, SLOT(close()));
    connect(&_apply_btn, SIGNAL(clicked()), this, SLOT(applyChanges()));
    connect(&_pic_height_sb, SIGNAL(editingFinished()), this, SLOT(updatePicWidth()));
    connect(&_pic_width_sb, SIGNAL(editingFinished()), this, SLOT(updatePicHeight()));
    connect(&_black_white_cb, SIGNAL(stateChanged(int)), this, SLOT(updateCheckboxes(int)));
}

void settingsWnd::applyChanges()
{
    _depth = _depth_sb.value();
    _bailout = static_cast<double>(_bailout_sb.value());
    _picSize = QSize(_pic_width_sb.value(), _pic_height_sb.value());
    _useGradient = _useGradient_cb.isChecked();
    _bw = _black_white_cb.isChecked();
}
void settingsWnd::updateCheckboxes(int newState)
{
    switch(newState)
    {
    case Qt::CheckState::Checked:
        _useGradient_cb.setChecked(false);
        _useGradient_cb.setEnabled(false);
        break;
    case Qt::CheckState::Unchecked:
        _useGradient_cb.setEnabled(true);
        _useGradient_cb.setChecked(true);
        break;
    default:
        //Other possibility is PartiallyChecked, which does not apply to our case.
        //Should never reach here.
        break;
    }
}
//These two functions are called whenever either the width or height of the pic are modified. They make sure that width/height = _picSizeProportion always.
void settingsWnd::updatePicHeight()
{
    _pic_height_sb.setValue(static_cast<int>(static_cast<double>(_pic_width_sb.value())/_picSizeProportion));
}
void settingsWnd::updatePicWidth()
{
    _pic_width_sb.setValue(static_cast<int>(_picSizeProportion * static_cast<double>(_pic_height_sb.value())));
}
