#include "burningshipcalculator.h"

burningShipCalculator::burningShipCalculator(PlaneManager *m)
    : fractalCalculator(m)
{
    _colorizer = std::shared_ptr<colorCalculator>(new colorCalculator(_depth));
}
burningShipCalculator::burningShipCalculator(burningShipCalculator &other)
    : fractalCalculator(other.getPlaneManager())
{
    _depth = other.getDepth();
    _bailoutRadius = other.getBailoutRadius();
    _colorizer = std::shared_ptr<colorCalculator>(new colorCalculator(_depth));
    _manager = other.getPlaneManager();
    _bailoutSq = _bailoutRadius*_bailoutRadius;
}

void burningShipCalculator::compute()
{
    QRgb color;
    QPointF c(0.0, 0.0);
    int width = _manager->getSize().width(), height = _manager->getSize().height();
    int it_num = 0;
    double zn = 0.0;
    //for each pixel (px, py) we map it to a point in the complex plane, using the plane manager. That point is the starting point of our iteration.
    for(int x = 0; x < width; ++x)
    {
            for(int y = 0; y < height; ++y)
            {
                c = _manager->mapFromPic(x, y);
                it_num = computePixel(c.x(), c.y(), &zn);
                color = _colorizer->computeColor(it_num, zn);
                _manager->colorPixel(x, y, color);
            }
    }
    emit doneComputing();
}
int burningShipCalculator::computePixel(const double& c_real, const double& c_im, double* zn)
{
    *zn = 0.0;
    double z0_real = 0, z1_real = 0.0, z1_imag = 0.0, modSquared = 0.0;
    //no optimization for burning ship;
    for(int it_num = 1; it_num <= _depth; ++it_num)
    {
        z0_real = z1_real;
        z1_real = z1_real*z1_real - z1_imag*z1_imag + c_real; //z = z0^2 + c ==>(Re) Re(z) = Re(z0)^2 - Im(z0)^2 + Re(c)
        z1_imag = 2.0*std::fabs(z0_real)*std::fabs(z1_imag) + c_im; //z = z0^2 + c ==>(Im) Im(z) = 2*Re(z0)*Im(z0) + Im(c);
        modSquared = z1_real*z1_real + z1_imag*z1_imag;
        if(modSquared > _bailoutSq)
        {
            *zn = std::sqrt(modSquared);
            return it_num;
        }
    }
    return _depth;
}
