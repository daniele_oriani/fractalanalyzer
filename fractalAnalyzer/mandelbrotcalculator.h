/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * Description: implementation of the mandelbrot set calculator ("is a" fractalCalculator).
 *      For its peculiar symmetries, this is the only one of the three fractals that can be computed with some optimizations, with not too much effort.
 *      That is obtained detecting points lying in the main cardioid or in the secondary bulb;
 * Copyright: Daniele Oriani 2015
 */
#ifndef MANDELBROTCALCULATOR_H
#define MANDELBROTCALCULATOR_H


#include "fractalcalculator.h"
#include "colorcalculator.h"
#include <cmath> //sqrt

class mandelbrotCalculator : public fractalCalculator
{
public:
    mandelbrotCalculator(PlaneManager* m);
    mandelbrotCalculator(mandelbrotCalculator &other);
    ~mandelbrotCalculator();
    void compute();
    void setOptimized(const bool& b) { _optimize = b; }
    bool getOptimized() const { return _optimize; }

private:
    bool _optimize;

    int computePixel(const double& c_real, const double& c_im, double *zn); //returns the number of iterations it took to explode, or depth if it converges (within depth). Also writes to zn the value of the iteration just before exploding.
};

#endif // MANDELBROTCALCULATOR_H
