/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * File: main.cpp
 * Description: An application able to generate fractals (Mandelbrot set, Burning Ship, Julia set). The images can be exported as jpg or png files.
 *              Through the settings window it is possible to tweak specific settings taking part in the computation of the fractal.
 *              The analytic part of the program measures computation time for different configurations that the user can load.
 *              The data can then be exported and plotted to be compared with another application I wrote, fractalDataAnalyzer.
 *              This file contains the code to start the main loop of the program.
 * Copyright: Daniele Oriani 2015
 */

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
