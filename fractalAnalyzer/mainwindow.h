#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * File: mainwindow.h
 * Description: contains the definition of the MainWindow class, which is the main interface window of the program.
 * Naming Standard: (suffixes)
 *      _pb = push button
 *      _le = line edit
 *      _lab = label
 *      _rb = radio button
 *      _cb = checkbox
 *      _spin = spinbox
 * Copyright: Daniele Oriani 2015
 */

#include <iostream>
#include <memory> //std::shared_ptr

#include <QMainWindow>
#include <QApplication>
#include <QPushButton>
#include <QRadioButton>
#include <QButtonGroup>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>
#include <QDoubleSpinBox>
#include <QMessageBox>
#include <QFileDialog>

#include <QRegExp>

#include "planemanager.h"
#include "fractalcalculator.h"
#include "mandelbrotcalculator.h"
#include "burningshipcalculator.h"
#include "juliacalculator.h"

#include "settingswnd.h"
#include "analysiswnd.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
//UI objects
    QPushButton _runAnalysis_pb;
    QPushButton _showSettings_pb;
    QPushButton _showAbout_pb;
    QPushButton _showAboutQt_pb;
    QPushButton _quit_pb;
    QRadioButton _selectMandelbrot_rb;
    QRadioButton _selectShip_rb;
    QRadioButton _selectJulia_rb;
    QButtonGroup _fractType_radioButtons;

    QLabel _centerPoint_lab;
    QLabel _scale_lab;
    QLabel _optimize_lab;
    QLabel _juliaC_lab;
    QLabel _juliaPow_lab;
    QLineEdit _center_Re_le;
    QLineEdit _center_Im_le;
    QDoubleSpinBox _scale_spin;
    QCheckBox _optimize_cb;
    QLineEdit _JuliaC_Re_le;
    QLineEdit _JuliaC_Im_le;
    QDoubleSpinBox _JuliaPow_spin;
    QLabel _imagPart_lab;
    QLabel _imagPart2_lab;
    QPushButton _drawFractal_pb;
    QPushButton _addToAnalysis_pb;

    QLabel _status_lab;
//Other windows
    settingsWnd _settings;
    analysisWnd _analysis;
//Plane Manager
    std::shared_ptr<PlaneManager> _planeManager;
//Fractal calculators
    std::shared_ptr<mandelbrotCalculator> _mandelbrot;
    std::shared_ptr<burningShipCalculator> _burningShip;
    std::shared_ptr<juliaCalculator> _juliaSet;
//Pointer to fractal image, held by the plane manager
    QImage* _fractalImage;
//Private functions
    void drawWindow(const QSize wndSize);
    void makeConnections();
    bool loadPlaneSettings();
    bool loadMandelbrotParameters();
    bool loadShipParameters();
    bool loadJuliaParameters();
private slots:
    bool validateText(const QString& newTxt);
    bool loadFractalParameters();
    void drawBtn_clicked();
    void fractTypeChanged(int newFractType);
    void fractalComputed();
    void updateStatus(const QString& newStatus){ _status_lab.setText(newStatus); }
    void addBtn_clicked();
    void aboutBtn_clicked();
};

#endif // MAINWINDOW_H
