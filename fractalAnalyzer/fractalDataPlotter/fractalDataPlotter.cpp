/* Author: daniele.oriani@studenti.unimi.it
 * Date: 26/01/2015
 * Project: Fractal Analyzer
 * File: fractalDataPlotter.cpp
 * Description: A small program to plot data points on a plane. The points are then joined by straight lines.
 * Usage: This program is intended to use with Daniele Oriani's fractal analyzer application. This little program accepts the following format for parameters:
 *  fractalDataPlotter [OPTIONS] <data_text_files>
 *  Where <data_text_files> represents the text files, containing the data to be plotted (in the format: each row is composed by x and y values, separated by one space character). 
 *  Available options are: --xlabel=<x_axis_label>
 *                         --ylabel=<y_axis_label>
 */

#include <iostream>
#include <string>
#include <regex>
#include <vector>
#include <algorithm>

#include "dataReader.h"

#include "TApplication.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TLegend.h"

typedef std::vector<std::string> string_vec;

std::string get_Axislabel(const int& argc, char** argv, const std::regex& rx);
string_vec getFileNames(const int& argc, char** argv);

int main(int argc, char** argv)
{
  string_vec filenames = getFileNames(argc, argv);
  int dataFilesCount = filenames.size();
  if(argc < 2 || filenames.size() < 1)
  {
    std::cerr << "Usage: fractalDataPlotter [OPTIONS] <data_text_files>\nExiting...\n";
    return 1;
  }
  std::string x_lab = get_Axislabel(argc, argv, std::regex("--xlabel=(.+)"));
  std::string y_lab = get_Axislabel(argc, argv, std::regex("--ylabel=(.+)"));
  
  TApplication Root_App("app", 0, 0);
  TCanvas Root_Wnd("c1", "Fractal Data Plotter", 450, 250, 650, 650);
  Root_Wnd.cd();
  TAxis* X_Axis, * Y_Axis;
  TLegend legend(0.65,0.7,0.9,0.9);
  legend.SetHeader("Legend:");
  
  dataReader::matrix_vec x_vecs, y_vecs; /*x-vectors and y-vectors for all the input files provided.*/
  std::vector<int> dataCount; //number of data points in each of the input files.
  std::vector<TGraph> graphs;
  
  //We already know how many elements will each of these vectors contain, so we might as well reserve the memory.
  x_vecs.reserve(dataFilesCount);
  y_vecs.reserve(dataFilesCount);
  dataCount.reserve(dataFilesCount);
  graphs.reserve(dataFilesCount);
  
  dataReader reader;
  
  for(int i = 0, dataVecIndex = 0; i < dataFilesCount && dataVecIndex < dataFilesCount; ++i)
  //i is the index of the input file currently under examination.
  //dataVecIndex is the index in the vectors. The two are not equal if a file is discarded for whatever reason.
  {
    reader.clearStream();
    reader.setFilename(filenames.at(i));
    if(!reader.read(x_vecs, y_vecs))
    {
      std::cerr << "Error reading file " << filenames.at(i).c_str() << ". Ignoring it...\n";
      continue;
    }
    else
    {
      dataCount.push_back(x_vecs.at(dataVecIndex).size());
      graphs.push_back(TGraph(dataCount.at(dataVecIndex), &x_vecs.at(dataVecIndex).at(0), &y_vecs.at(dataVecIndex).at(0)));
      legend.AddEntry(&graphs.at(0)+dataVecIndex,filenames.at(i).c_str(),"lep");
      ++dataVecIndex;
      std::cout << "Successfully read file no. " << 1+i << std::endl;
    }
  }
  
  int graphsCount = graphs.size();
  short int color=1;
  const short int marker=20;
  std::vector<double> minimaY, maximaY, minimaX, maximaX;
  for(int j = 0; j < graphsCount; ++j)
  {
      if(j==0)
      {
          graphs.at(j).SetMarkerStyle(marker);
          graphs.at(j).SetMarkerColor(color);
          graphs.at(j).SetLineColor(color);
          graphs.at(j).SetTitle("");
          graphs.at(j).Draw("APL");
          X_Axis = graphs.at(0).GetXaxis();
          Y_Axis = graphs.at(0).GetYaxis();
      }
      else
      {
          graphs.at(j).SetMarkerStyle(marker);
          graphs.at(j).SetMarkerColor(color);
          graphs.at(j).SetLineColor(color);
          graphs.at(j).Draw("PL");
      }
      minimaY.push_back(*std::min_element(y_vecs.at(j).begin(), y_vecs.at(j).end()));
      maximaY.push_back(*std::max_element(y_vecs.at(j).begin(), y_vecs.at(j).end()));
      minimaX.push_back(*std::min_element(x_vecs.at(j).begin(), x_vecs.at(j).end()));
      maximaX.push_back(*std::max_element(x_vecs.at(j).begin(), x_vecs.at(j).end()));
      ++color;
      
  }
  X_Axis->SetTitle(x_lab.c_str());
  Y_Axis->SetTitle(y_lab.c_str());
  X_Axis->CenterTitle();
  Y_Axis->CenterTitle();

  double minY, maxY, minX, maxX;
  minY = *std::min_element(minimaY.begin(), minimaY.end());
  maxY = *std::max_element(maximaY.begin(), maximaY.end());
  minX = *std::min_element(minimaX.begin(), minimaX.end());
  maxX = *std::max_element(maximaX.begin(), maximaX.end());
  
  double paddingY = (maxY-minY)/20.;
  double paddingX = (maxX-minX)/20.;
  
  Y_Axis->SetRangeUser(minY-paddingY, maxY+paddingY);
  X_Axis->SetRangeUser(minX-paddingX, maxX+paddingX);
  legend.Draw();
  
  std::cout << "Select Quit from menu to exit..." << std::endl;
  Root_App.Run(kTRUE);
  return 0;
}

std::string get_Axislabel(const int& argc, char** argv, const std::regex& rx)
{
  //returns empty string if it fails;
  std::string res;
  std::smatch rx_match_results;
  for(int i = 1; i < argc; ++i)
  {
    std::regex_match(std::string(argv[i]), rx_match_results, rx);
    if(rx_match_results.size() > 1) //first result is the entire expression. We want to catch the value after =
    {
      res = rx_match_results.str(1);
      res.erase(res.begin()); //something weird with <regex> : there seems to be an extra character in the catched group... Need to erase it.
    }
  }
  return res;
}
string_vec getFileNames(const int& argc, char** argv)
{
  std::regex rx("--.*");
  string_vec res;
  for(int i = 1; i < argc; ++i)
  {
    if(!std::regex_match(argv[i], rx))
      res.push_back(std::string(argv[i]));
  }
  return res;
}
