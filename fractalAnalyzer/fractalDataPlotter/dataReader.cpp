#include "dataReader.h"

dataReader::dataReader(const std::string& filename)
  : _filename(filename)
{
  _stream.open(_filename);
}
dataReader::dataReader(char* filename)
  : _filename(std::string(filename))
{
  _stream.open(_filename);
}

bool dataReader::read(dataReader::matrix_vec& x_vecs, dataReader::matrix_vec& y_vecs)
{
  bool success = false;
  dataReader::vec_type cur_x_vec, cur_y_vec;
  //if no file is set or if the stream is not open and trying to opening it fails, then we return false and do not add any new data;
  if(_filename.empty())
  {
    return success; //fails if no filename was set;
  }else if(!_stream.is_open())
  {
    _stream.open(_filename);
    if(!_stream.good()) return success; //fails if the file cannot be opened;
  }
  //If we get here we're good to go!
  while(_stream.good())
  {
    double temp;
    _stream >> temp;
    cur_x_vec.push_back(temp);
    
    if(!_stream.good()) //if number of entries is odd, or other stream errors occur while reading a couple (x, y), then interrupt reading and return what we got. Returns false;
    {
      cur_x_vec.pop_back();
      break;
    }
    _stream >> temp;
    cur_y_vec.push_back(temp);
  }
  _stream.close();
  
  success = true | _stream.good();
  
  if(success)
  {
    x_vecs.push_back(cur_x_vec);
    y_vecs.push_back(cur_y_vec);
  }
  return success;
}

dataReader::~dataReader()
{
  if(_stream.is_open())
    _stream.close();
}
