/* Author: daniele.oriani@studenti.unimi.it
 * Date: 26/01/2015
 * Project: Fractal Analyzer
 * File: dataReader.h
 * Description: A small class that reads the text files, knowing the output format of the Fractal Analyzer application.
                In the main application there are two vectors. One contains the x-vectors for all the input files provided. The other one contains the y-vectors for all the input files provided.
                The read function works with pass by ref, so no memory should be wasted.
                To use the same object of this class with more files subsequently, use the clearStream() function when done with each file.
 */
#ifndef _DATAREADER_H
#define _DATAREADER_H

#include <fstream>
#include <vector>
#include <string>

class dataReader
{
  public:
    typedef std::vector<double> vec_type;
    typedef std::vector<dataReader::vec_type> matrix_vec; //vector of vectors: a matrix_vec contains e.g. as elements the x-vectors of all the input files.
    dataReader() {  }
    dataReader(const std::string& filename);
    dataReader(char* filename);
    ~dataReader();
    bool read(dataReader::matrix_vec& x_vecs, dataReader::matrix_vec& y_vecs);
    /*This function writes in already existing matrix_vecs. It reads the x-vector and y-vector from the current file and then adds them to the matrix_vecs passed (only if it succeeds)*/
    void setFilename(const std::string& filename) { _filename = filename; }
    void setFilename(char* filename) { _filename = std::string(filename); }
    void clearStream() { _stream.close(); _stream.clear(); } //resets the stream;
    std::fstream::iostate getStreamState() const { return _stream.rdstate(); }
   private:
    std::ifstream _stream;
    std::string _filename;
};

#endif //_DATAREADER_H
