/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * File: fractalAnalysisSetUp.h
 * Description: a header file, containing the definition of two structs. fractalAnalysisSetUp holds the properties of a fractal, and planeSetUp holds
 *              the properties of the plane.
 * Copyright: Daniele Oriani 2015
 */
#ifndef FRACTALANALYSISSETUP_H
#define FRACTALANALYSISSETUP_H

#include <QSize>
#include <QPointF>

struct fractalAnalysisSetUp
{
    enum struct fractalType { Mandelbrot, BurningShip, JuliaSet, None };

    fractalType type;
    int depth;
    double bailout;
    QSize picSize;
    bool useGradient;
    bool bw;
    bool optimize;
};

struct planeSetUp
{
    QPointF center;
    double scale;
};
#endif // FRACTALANALYSISSETUP_H
