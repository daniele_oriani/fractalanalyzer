#include "juliacalculator.h"

juliaCalculator::juliaCalculator(PlaneManager* m)
    :fractalCalculator(m)
{
    _pow = 2.0;
    _c = QPointF(0.0, 0.0);
    _colorizer = std::shared_ptr<colorCalculator>(new colorCalculator(_depth));
}
juliaCalculator::juliaCalculator(juliaCalculator &other)
    : fractalCalculator(other.getPlaneManager())
{
    _depth = other.getDepth();
    _bailoutRadius = other.getBailoutRadius();
    _colorizer = std::shared_ptr<colorCalculator>(new colorCalculator(_depth));
    _manager = other.getPlaneManager();
    _bailoutSq = _bailoutRadius*_bailoutRadius;
    _pow = other.getPower();
    _c = other.getC();
}
void juliaCalculator::compute()
{
    QRgb color;
    int width = _manager->getSize().width(), height = _manager->getSize().height();
    int it_num = 0;
    QPointF zn(0.0, 0.0);
    double lastItZn = 0.0;
    //for each pixel (px, py) we map it to a point in the complex plane, using the plane manager. That point is the starting point of our iteration.
    for(int x = 0; x < width; ++x)
    {
            for(int y = 0; y < height; ++y)
            {
                zn = _manager->mapFromPic(x, y);
                it_num = computePixel(zn.x(), zn.y(), &lastItZn);
                color = _colorizer->computeColor(it_num, lastItZn);
                _manager->colorPixel(x, y, color);
            }
    }
    emit doneComputing();
}
int juliaCalculator::computePixel(const double& z_real, const double& z_im, double* zn)
{
    *zn = 0.0;
    std::complex<double> z(z_real, z_im);
    std::complex<double> c(_c.x(), _c.y());
    double modSquared = 0.0;
    //no optimization for julia set;
    for(int it_num = 1; it_num <= _depth; ++it_num)
    {
        z = std::pow(z, _pow) + c;
        modSquared = z.real()*z.real() + z.imag()*z.imag();
        if(modSquared > _bailoutSq)
        {
            *zn = std::norm(z);
            return it_num;
        }
    }
    return _depth;
}
