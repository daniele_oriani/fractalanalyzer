/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * Description: implementation of the burning ship fractal calculator ("is a" fractalCalculator).
 *      The popular shape of the ship is only obtained if the final image is upside-down.
 * Copyright: Daniele Oriani 2015
 */
#ifndef BURNINGSHIPCALCULATOR_H
#define BURNINGSHIPCALCULATOR_H

#include "fractalcalculator.h"
#include "colorcalculator.h"
#include <cmath> //sqrt

class burningShipCalculator : public fractalCalculator
{
public:
    burningShipCalculator(PlaneManager* m);
    burningShipCalculator(burningShipCalculator& other);
    ~burningShipCalculator() {  }

    void compute();
private:
    int computePixel(const double& c_real, const double& c_im, double *zn); //returns the number of iterations it took to explode, or depth if it converges (within depth). Also writes to zn the value of the iteration just before exploding.
};

#endif // BURNINGSHIPCALCULATOR_H
