#include "fractalcalculator.h"

fractalCalculator::fractalCalculator(PlaneManager *m)
    : QThread(), _manager(m)
{
    _depth = 1000;
    _bailoutRadius = 512.0;
    _bailoutSq=_bailoutRadius*_bailoutRadius;
}
fractalCalculator::~fractalCalculator()
{

}
