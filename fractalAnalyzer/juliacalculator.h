/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * Description: implementation of the Julia set calculator ("is a" fractalCalculator).
 *      The idea behind this fractal is mathematically quite different from that of the burning ship and the mandelbrot set.
 *      Because of this the implementation of the calculator differs a little from the others.
 * Copyright: Daniele Oriani 2015
 */
#ifndef JULIACALCULATOR_H
#define JULIACALCULATOR_H

#include "fractalcalculator.h"
#include "colorcalculator.h"
#include <cmath> //sqrt
#include <complex>

class juliaCalculator : public fractalCalculator
{
public:
    juliaCalculator(PlaneManager* m);
    juliaCalculator(juliaCalculator &other);
    ~juliaCalculator() {}

    void compute();

    void setC(const QPointF& c) { _c = c; }
    void setC(const double& x, const double& y) { _c.setX(x); _c.setY(y); }
    void setPower(const double& p) { _pow = p; }

    QPointF getC() const { return _c; }
    double getPower() const { return _pow; }
private:
    double _pow;
    QPointF _c; //constant of the f(z) rational complex function;
    int computePixel(const double& z_real, const double& z_im, double *zn); //returns the number of iterations it took to explode, or depth if it converges (within depth). Also writes to zn the value of the iteration just before exploding.
};

#endif // JULIACALCULATOR_H
