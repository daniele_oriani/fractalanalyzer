#include "colorcalculator.h"

colorCalculator::colorCalculator(const int &max_it_num)
    :_depth(max_it_num), _ln2(std::log(2.0))
{
    _bw = false;
    _useGradient = true;
    _distribute = std::log;
    _a = 0.95;
    _b = -39.0;
    _gradientDensity = 23.0;
    _foreColor = QColor(0, 0, 0).rgb(); //black
    _backgroundColor = QColor(255, 255, 255).rgb(); //white;
    initPalette();
}
void colorCalculator::initPalette()
{
    //The palette for this gradient is pretty big, so I preferred to put its initialization in a separate file.
    //I named it with extention .C to denote that it is just a part of code, not anything meaningful on its own.

    #include "gradient.C"
}
QRgb colorCalculator::computeColor(const int& it_num, const double& zn)
{
    if(it_num == _depth) //if it_num == maximum iterations number ==> return black (we're inside the set);
    {
        return _foreColor;
    }
    if(_bw) //black or white: either we're in the set or we are not;
    {
        return _backgroundColor;
    }

    QRgb res;//the result;
    double hue = it_num + 1 - std::log(std::log(zn))/_ln2; //the so called real iteration number;

    if(_useGradient)
    {
        hue *= _gradientDensity;
        int colorIndex = static_cast<int>(hue)%PALETTE_SIZE; //get the index of the element of the gradient palette we want;
        res = _palette[colorIndex].rgb(); //return the corresponding color;
    }
    else
    {
        hue = _a + _b * (*_distribute)(hue); //compute the HSV hue term, with shift _a, coeff _b and distribution _distribute
        while(hue > 360)
                hue -=360.0;
        while(hue < 0.0)
                hue += 360.0;
        //get the corresponding color from the HSV color space and return it;
        res = QColor::fromHsvF(hue/360.0, 0.78, 0.94).rgb();
    }

    return res;
}
