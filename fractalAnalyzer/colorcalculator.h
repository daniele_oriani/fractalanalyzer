/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * File: colorcalculator.h
 * Description: this is the parent class for all color calculators. It provides the possibility to use a gradient, a hue distribution or to just color
 *              the fractal in black and white. The only color calculator implemented in this project is the one based on the normalized iteration count
 *              algorithm.
 * Copyright: Daniele Oriani 2015
 */
#ifndef COLORCALCULATOR_H
#define COLORCALCULATOR_H

#define PALETTE_SIZE 2048

#include <QColor>
#include <cmath> //std::log

class colorCalculator
{
public:
    typedef double (*distribution_t)(double);
    colorCalculator(const int& max_it_num);

    QRgb computeColor(const int& it_num, const double& zn);

    bool getBw() const { return _bw; } //Is B/W mode on?
    void setBw(const bool& b) { _bw = b; } //Set B/W mode;
    void setUseGradient(const bool& b) { _useGradient = b; }
    bool useGradient() const { return _useGradient; }
    void setGradientDensity(const double& d) { _gradientDensity = d; }
    double getGradientDensity() const { return _gradientDensity; }
    void setDistribution(double (*func)(double)) { _distribute = func; } //set pointer to a mathematical function, e.g. std::log;
    distribution_t getDistribution() const { return _distribute; } //get pointer to mathematical function _distribute;
    void setDistributionCoefficients(const double& a, const double& b) { _a = a; _b = b; }
    void getDistributionCoefficients(double* a, double* b) { *a = _a; *b = _b; }
    //the distribution is used as alternative to the gradient;
    void setDepth(const int& max_it_num){ _depth = max_it_num; }
    int getDepth() const { return _depth; }

    ~colorCalculator() {}
private:
    void initPalette();
protected:
    int _depth; //max num of iterations;
    bool _useGradient;
    bool _bw; //black & white;
    QRgb _backgroundColor, _foreColor; //default: white, black;
    distribution_t _distribute; //pointer to a function from which to extract values for the hue;
    double _a, _b; //distribution coefficients;
    double _gradientDensity; //color density for default gradient;
    QColor _palette[PALETTE_SIZE]; //holds the colors of the gradient;
    const double _ln2;
};

#endif // COLORCALCULATOR_H
