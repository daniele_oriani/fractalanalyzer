/*
 * File: planemanager.h
 * Author: daniele.oriani@studenti.unimi.it
 * Description: A class, using Qt library objects, to manage a QImage, enabling to treat it as a complex plane.
 *   Bounds represent the maxima and minima of what you want to represent on the plane.
 *   Read the comment about the resize method: it has drastic consequences.
 *   Inheritance from QObject and use of the Q_OBJECT macro are necessary to use the signals/slots functionalities.
 *   The class holds the actual QImage representing the fractal. Try to only use pointers to this object, avoiding copying if at all possible.
*/
#ifndef PLANEMANAGER_H
#define PLANEMANAGER_H

#include <QObject>
#include <QImage>
#include <QColor>

#include <cmath>

class PlaneManager : public QObject
{
    Q_OBJECT
public:
    PlaneManager(const QSize& s=QSize(1600, 1200), const double& minX=-2.0, const double& minY=-1.0, const double& maxX=0.5, const double& maxY=1.0, const QPointF& center=QPointF(0.0, 0.0));

    QImage* getPic() { return &_pic; }
    QSize getSize() const { return _size; }
    double getBound_xMin() const { return _minX; }
    double getBound_xMax() const { return _maxX; }
    double getBound_yMin() const { return _minY; }
    double getBound_yMax() const { return _maxY; }
    QPointF getCenter() const { return _cent; }
    double getScaleFactor() const { return _scaleFactor; }
    bool getBadBounds() const { return _badBounds; }
    bool getBadScale() const { return _badScale; }

    void setBounds(const double& minX, const double& minY, const double& maxX, const double& maxY)
                    { _minX = minX; _minY = minY; _maxX = maxX; _maxY = maxY; emit rescaled(); emit resized(); }
    void setCenter(const QPointF& c) { _cent = c; }
    void setCenter(const double& re, const double& im) { _cent = QPointF(re, im); }

    /*VERY IMPORTANT: if you need to both resize and scale the plane, use scale first and then resize!*/
    void resize(const QSize& newSize);
    void scale(const double& s);

    /*Maps a point of the complex plane to the pixel coordinates of the image;*/
    QPoint mapToPic(const QPointF& X);
    QPoint mapToPic(const double& x, const double& y);
    /*Returns the point of the complex plane corresponding to the given pixel coordinates;*/
    QPointF mapFromPic(const QPoint& P);
    QPointF mapFromPic(const int& px, const int& py);
    /*Sets the wanted color to a pixel*/
    void colorPixel(const QPoint& pixel, const QRgb& color) { _pic.setPixel(pixel, color); }
    void colorPixel(const int& pixelX, const int& pixelY, const QRgb& color) { _pic.setPixel(pixelX, pixelY, color); }
private:
    QImage _pic;
    QSize _size;
    double _minX, _minY, _maxX, _maxY;
    double _pixels_per_unit_X,_pixels_per_unit_Y;
    double _scaleFactor;
    QPointF _cent; //center of the plane: a complex number;

    QRgb _defaultForeColor, _defaultBackgroundColor;
    bool _badBounds; //true if bad values are set for bounds: expect garbage!
    bool _badScale; //true if bad values are set for scale: expect garbage!
    int roundToNearestInt(const double& d) { return std::floor(0.5 + d); }
signals:
 //emitted when resize is called, on rescale, or when bounds change;
    void resized();
    void rescaled();
private slots:
    void _update_pixels_per_unit(); //called when resized() is emitted;
    void _update_bounds(); //called when rescaled() is emitted;
};

#endif // PLANEMANAGER_H
