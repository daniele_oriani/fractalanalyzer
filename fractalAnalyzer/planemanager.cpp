#include "planemanager.h"

PlaneManager::PlaneManager(const QSize& s, const double& minX, const double& minY, const double& maxX, const double& maxY, const QPointF& center)
    : _size(s), _minX(minX), _minY(minY), _maxX(maxX), _maxY(maxY), _cent(center)
{
    _badBounds =false;
    _badScale = false;
    _scaleFactor = 1.0;
    _defaultForeColor = qRgb(0, 0, 0);
    _defaultBackgroundColor = qRgb(255, 255, 255);
    _pic = QImage(_size, QImage::Format_ARGB32);
    _pic.fill(0u); //always better to initialize completely the qimage;

    connect(this, SIGNAL(resized()), this, SLOT(_update_pixels_per_unit()));
    connect(this, SIGNAL(rescaled()), this, SLOT(_update_bounds()));

    _update_pixels_per_unit();
}
QPoint PlaneManager::mapToPic(const QPointF& X)
{
    QPoint res;
    double px, py;
    px = _pixels_per_unit_X*(X.x() - _cent.x()) + static_cast<double>(_size.width())*0.5;
    py = _pixels_per_unit_Y*(_cent.y() - X.y()) + static_cast<double>(_size.height())*0.5;
    res.setX(roundToNearestInt(px));
    res.setY(roundToNearestInt(py));
    return res;
}
QPoint PlaneManager::mapToPic(const double& x, const double& y)
{
    QPoint res;
    double px, py;
    px = _pixels_per_unit_X*(x - _cent.x()) + static_cast<double>(_size.width())*0.5;
    py = _pixels_per_unit_Y*(_cent.y() - y) + static_cast<double>(_size.height())*0.5;
    res.setX(roundToNearestInt(px));
    res.setY(roundToNearestInt(py));
    return res;
}
QPointF PlaneManager::mapFromPic(const QPoint& P)
{
    QPointF res;
    double x, y;
    x = _cent.x() + ((static_cast<double>(P.x()) - static_cast<double>(_size.width())*0.5)/_pixels_per_unit_X);
    y = _cent.y() + ((static_cast<double>(_size.height())*0.5 - static_cast<double>(P.y()))/_pixels_per_unit_Y);
    res.setX(x);
    res.setY(y);
    return res;
}
QPointF PlaneManager::mapFromPic(const int& px, const int& py)
{
    QPointF res;
    double x, y;
    x = _cent.x() + ((static_cast<double>(px) - static_cast<double>(_size.width())*0.5)/_pixels_per_unit_X);
    y = _cent.y() + ((static_cast<double>(_size.height())*0.5 - static_cast<double>(py))/_pixels_per_unit_Y);
    res.setX(x);
    res.setY(y);
    return res;
}
void PlaneManager::_update_pixels_per_unit()
{
    //we're confident that anybody who could be using this class will be smart enough not to put dumb values of _maxX ecc. But...
    if(_maxX - _minX > 0. && _maxY - _minY > 0.)
    {
        _pixels_per_unit_X = static_cast<double>(_size.width())/(_maxX - _minX);
        _pixels_per_unit_Y = static_cast<double>(_size.height())/(_maxY - _minY);
        _badBounds = false;
    }
    else
    {
        _pixels_per_unit_X = 1.;
        _pixels_per_unit_Y = 1.;
        _badBounds = true;
    }
}
void PlaneManager::_update_bounds()
{
    _maxX /= _scaleFactor;
    _minX /= _scaleFactor;
    _maxY /= _scaleFactor;
    _minY /= _scaleFactor;
}
void PlaneManager::resize(const QSize& newSize)
{
    _size = newSize;
    _pic = QImage(_size, QImage::Format_ARGB32);
    emit resized();
}
void PlaneManager::scale(const double& s)
{
    if(s > 0)
    {
        _maxX *= _scaleFactor;
        _minX *= _scaleFactor;
        _maxY *= _scaleFactor;
        _minY *= _scaleFactor;
        _scaleFactor = s;
        _badScale = false;
        emit rescaled();
    }
    else
    {
        _scaleFactor = 1.;
        _badScale = true;
        emit rescaled();
    }
}
