/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * File: analysiswnd.h
 * Description: this window stores different configurations that the user wants to test. The configurations are added through the mainwindow interface.
 *              When analysis starts the fractals for each configuration are computed one after the other. The computation time for each one is measured and saved.
 *              It is then possible to export the results as a text file, with a very simple format.
 *              That text file can be used as a data source with the plotter program.
 * Naming Standard: (suffixes)
 *      _pb = push button
 *      _txt = text edit
 *      _lab = label
 *      _combo = combo box
 *      _cb = checkbox
 * Copyright: Daniele Oriani 2015
 */

#ifndef ANALYSISWND_H
#define ANALYSISWND_H

#include <QDialog>
#include <QApplication>
#include <QProcess>
#include <QMessageBox>
#include <QFileDialog>

#include <QPlainTextEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QComboBox>

#include <fstream>
#include <tuple>
#include <vector>
#include <memory> //std::shared_ptr
#include <chrono>

#include <unistd.h>

#include "fractalAnalysisSetUp.h"
#include "fractalcalculator.h"
#include "planemanager.h"
#include "mandelbrotcalculator.h"
#include "burningshipcalculator.h"
#include "juliacalculator.h"

class analysisWnd : public QDialog
{
    Q_OBJECT

public:
    analysisWnd(QWidget *parent = 0);
    ~analysisWnd();
    enum class IndependentVariable { N, Bailout, Depth, Width, ScaleFactor }; //N is the index of the config;

    void addSetUpElement(const fractalAnalysisSetUp& settings, const planeSetUp& planeSettings) { _setups.push_back(std::make_tuple(settings, planeSettings)); }
    /*the fractalCalculator _currentFract holds the info regarding the specific fractal (zoom, center, ecc), because it can return a pointer to the plane manager;*/
public slots:
    void clear() { _setups.clear(); showElementsList(); }
    void removeLast()
    {
        if(!_setups.empty())
        {
            _setups.pop_back();
            showElementsList();
        }
    }
    void showElementsList(); //display what configs we have ready to be analyzed;
    void run();
private slots:
    void fractalComputed();
    void startNextComputation(unsigned int computationIndex);
    void setIndependentVar(int newVar);
    void getDataReadyToExport();
    void exportData(const std::vector<double>& x, const std::vector<double>& t);
private:  
    void drawWnd();
    void makeConnections();

    QString getSetupInfo(const std::tuple<fractalAnalysisSetUp, planeSetUp > &setup);
    QString boolToStr(const bool& b) { return b ? QString("True") : QString("False"); }

    void showSavePicsDialog(bool* abortAnalysis);
    void savePic(unsigned int fractalIndex);

    void startPlotter(const QString& filename);

    std::shared_ptr<fractalCalculator> _currentFract; //pointer to the fractal currently computing.
    PlaneManager _planeManager; //the plane. It's separate from the mainwindow plane, because they are different.

    std::vector< std::tuple<fractalAnalysisSetUp, planeSetUp> > _setups; //vector of couples of two objects, representing together one configuration.

    QString _savePath; //where we'll save the pics;
    bool _savePics; //wheter we'll save them;

    std::chrono::steady_clock::time_point _clock_start, _clock_end; //my stopwatch;

    std::vector<double> _results; //computation time for each config;

    IndependentVariable _x_type; //time is the dependent var (y). But the user actually has a choice on what should be on the x-axis;

    QPlainTextEdit analysisTextInfo_txt;
    QPushButton run_pb;
    QPushButton clear_pb;
    QPushButton removeLast_pb;
    QCheckBox _exportData_cb;
    QLabel _variable_lab;
    QComboBox _choosex_combo;
};

#endif // ANALYSISWND_H
