#ifndef FRACTALCALCULATOR_H
#define FRACTALCALCULATOR_H

/*
 * Author: daniele.oriani@studenti.unimi.it
 * File: fractalcalculator.h
 * Description: base class for the implementations of the fractal generators (Julia Set, Burning Ship, Mandelbrot).
 *  The constructor needs a pointer to the plane manager. That is because the fractal generators need to know the parameters of the image and they need to tell
 *  the plane manager the color of every pixel of the image.
 *  The depth represents the maximum number of iterations the algorithm does before deciding that the current point converges.
 *  The bailout radius represents how far from the origin our iterations can take us, before we decide to stop and consider that point as our final iteration.
 * The generators color the image through a colorCalculator, which in our case is the normalized iteration count algorithm.
 * The color is decided looking at how rapidly the point has diverged. Black means convergence.
*/

#include "planemanager.h"
#include "colorcalculator.h"

#include <memory> //std::shared_ptr
#include <QThread>

class fractalCalculator: public QThread
{
    Q_OBJECT
public:
    fractalCalculator(PlaneManager* m);
    void setDepth(const int& d) { _depth = d; _colorizer->setDepth(d);}
    int getDepth() const { return _depth; }
    void setBailoutRadius(const double& r) { _bailoutRadius = r; _bailoutSq = r*r; }
    double getBailoutRadius() const { return _bailoutRadius; }

    colorCalculator* getColorizer() { return _colorizer.get(); } //return regular pointer to _colorizer;
    PlaneManager* getPlaneManager() { return _manager; }

    virtual void run() { compute(); } //overrides QThread::run()
    virtual ~fractalCalculator();
signals:
    void doneComputing();
protected:
    virtual void compute() = 0;

    PlaneManager* _manager;
    int _depth;
    double _bailoutRadius;
    double _bailoutSq;
    std::shared_ptr<colorCalculator> _colorizer;
};

#endif // FRACTALCALCULATOR_H
