/*
 * Author: daniele.oriani@studenti.unimi.it
 * Project: Fractal Analyzer
 * File: settingswnd.h
 * Description: this window offers the possibility to modify some of the most important parameters for the fractal computed. Namely:
 *              - Depth
 *              - Bailout Radius
 *              - Picture size
 *              - Use of a gradient (instead of a color distribution operating in the HSV space)
 *              - Black/White (overrides gradient)
 *              These settings were separated from the rest on the mainwindow, because the ones that appear there affect the subject of the image,
 *              where the ones here affect the quality or other parameters.
 * Naming Standard: (suffixes)
 *      _btn = push button
 *      _lab = label
 *      _sb = spin box
 *      _cb = checkbox
 * Copyright: Daniele Oriani 2015
 */
#ifndef SETTINGSWND_H
#define SETTINGSWND_H

#include <QDialog>
#include <QPushButton>
#include <QSpinBox>
#include <QLabel>
#include <QCheckBox>

#include "fractalAnalysisSetUp.h"

class settingsWnd : public QDialog
{
    Q_OBJECT

public:
    settingsWnd(QWidget *parent = 0);
    int getDepth() const { return _depth; }
    double getBailoutRadius() const { return _bailout; }
    QSize getPicSize() const { return _picSize; }
    bool getUseGradient() const { return _useGradient; }
    bool getUseBlackWhite() const { return _bw; }
    fractalAnalysisSetUp getSettings()
    {
        fractalAnalysisSetUp res;
        res.type = fractalAnalysisSetUp::fractalType::None;
        res.depth = _depth;
        res.bailout = _bailout;
        res.picSize = _picSize;
        res.bw = _bw;
        res.useGradient = _useGradient;
        res.optimize = true; //default value: will be modified by mainwindow.
        return res;
    }
private:
    QPushButton _apply_btn;
    QPushButton _close_btn;
    QLabel _depth_lab;
    QLabel _bailout_lab;
    QLabel _useGradient_lab;
    QLabel _blackWhite_lab;
    QLabel _picSize_lab;

    QSpinBox _depth_sb;
    QSpinBox _bailout_sb;
    QSpinBox _pic_width_sb;
    QSpinBox _pic_height_sb;
    QCheckBox _black_white_cb;
    QCheckBox _useGradient_cb;

    int _depth;
    double _bailout;
    QSize _picSize;
    bool _useGradient;
    bool _bw;
    const double _picSizeProportion; //  width/height;

    void drawWnd();
    void makeConnections();

private slots:
    void applyChanges();
    void updatePicHeight();
    void updatePicWidth();
    void updateCheckboxes(int); //in order for their values to make sense;
};

#endif // SETTINGSWND_H
