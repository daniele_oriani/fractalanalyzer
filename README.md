# README #

An application able to generate fractals (Mandelbrot set, Burning Ship, Julia set). The images can be exported as jpg or png files.
Through the settings window it is possible to tweak specific settings taking part in the computation of the fractal.
The analytic part of the program measures computation time for different configurations that the user can load.
The data can then be exported and plotted to be compared with another application I wrote, fractalDataAnalyzer.

The program was a project developed as a numerical data treatment exam. It still has some unresolved issues. Not hard to fix, but I don't have the time at the moment to get back to work on this. Anyway, I'm putting this out there, in case anyone finds my work useful.

### How do I get set up? ###
The program was developed on a Linux platform. I have not tested it on anything else.
It was written using Qt Creator with Qt 5.3 and C++11. Thus you will need the Qt libraries and a modern C++ compiler (like g++).
Also an attached utility makes use of the ROOT libraries by Cern to plot the computation time data. The main program can perfectly function without even compiling this part tho.

To check the program out you will need to build it. To do so, go to the Release directory and run the command
make
If your set up is good it will build everything it needs and you'll be able to run the program. If you run into problems you might want to try to go to the directory containing the source code and running the command
qmake
Then try to run
make
again. Everything should work then.

### Contacts ###

Author: daniele.oriani.gm@gmail.com